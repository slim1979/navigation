Rails.application.routes.draw do
  resources :navigates, only: :index do
    patch :proceed, on: :collection
  end

  get '/navigates/proceed', to: 'navigates#proceed'
  root 'navigates#index'
end
