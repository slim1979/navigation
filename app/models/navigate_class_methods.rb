module NavigateClassMethods
  def proceed(params)
    root_directory = params[:root_directory]
    path_to        = params[:path_to]

    change_root(root_directory)     unless root_directory.blank?
    objects = show_objects(path_to) unless path_to.blank?
    objects
    # debugger
  end

  def current_root
    return unless File.exist?('root_directory.txt')

    zero           = File.zero?('root_directory.txt')
    root_directory = zero ? '' : File.open('root_directory.txt', &:readline)
    root_directory
  end

  def change_root(root_directory)
    File.open('root_directory.txt', 'w') do |f|
      f << root_directory
    end
  end

  def show_objects(path_to)
    return unless path_to.match? %r{^[a-zA-Z]{3,}\/{1}.*\/?$}

    path_to = path_to.split('/')
    type    = path_to.first
    kind    = path_to[1..-1].join('/')

    assign_values(type, kind)
  end

  def assign_values(type, kind)
    root_directory = current_root
    return if root_directory.blank?

    path = root_directory + kind
    return unless File.directory? path

    @attrs = {}
    @attrs[:current_path] = kind
    @attrs[:type] = type
    Dir.chdir(path) do
      if type == 'dir'
        @attrs[:objects] = Dir.glob('*').select { |f| File.directory? f }
      elsif type == 'files'
        @attrs[:objects] = Dir.glob('*.*')
      end
    end
    @attrs
  end
end
