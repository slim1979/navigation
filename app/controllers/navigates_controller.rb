class NavigatesController < ApplicationController
  before_action :load_current_root

  def index; end

  def proceed
    result = Navigate.proceed(params)
    return unless result

    @objects      = result[:objects]
    @current_path = result[:current_path]
    @type         = result[:type]
  end

  private

  def load_current_root
    @current_root = Navigate.current_root
  end
end
